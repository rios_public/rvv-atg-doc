# RISC-V Vector Automatic Tests Generator (RVVATG)
[TOC]
## Introduction

RVVATG - The RISC-V Vector Automatic Tests Generator is the first test suite generator for RISC-V Vector extension. This is a project in collaboration with the RISC-V International Foundation and the produced RVV test suites will be adopted by RISC-V community as the official RVV test release. In order to verify the generated RVV tests, we also design and implement the first configurable RVV Sail model. Such a reference model will be utilized as the golden model for RISC-V testing and cross-validations.

RVVATG can automatically generate RVV assembly test suite based on the user configurations and produce coverage reports harnessing the RISCOF and RISC-V ISAC infrastructures for each executed test case. The RVVATG supports all the combinations of RVV configurations (i.e. `vlen`, `sew` and `lmul`),  user-defined test coverage points and customized instruction selections, featuring a high degree of configurability. The RVVATG is capable of cooperating with both the RISC-V Spike simulator and Sail model as the reference models for test suite verifications and coverage validations. Currently, all the test coverages with a `vlen` between 128 and 4096 have been extensively verified, which is aligned with the maximum `vlen` support in the RISC-V Spike simulator. The configurations with the `vlen` larger than 4096 are currently under verification using our RVV Sail golden model. 


## Overview

The following diagram captures the overall flow of RVVATG and its components. Components outside the box either denote user inputs or external tools that could hook up with the output. 
![](images/rvvatg.png)

### Inputs to the framework

As we can see in the image above, the framework requires two specific inputs from the users:
1. The `instruction`, and the relative `vlen`, `sew`, and `lmul`. 
2. The source value combination, `rs1` and `rs2`, are specified by RISC-V foundation.

### Components of the framework
1. **Test Generator** generates a full assembly test file with both head & tail and body
2. **Macro Generator** generates the user-defined instructions with customized `vlen`, `sew`, and `lmul` from python scripts.
3. **Coverage Report Generator** generates coverage reports, which contain register coverages and source value coverages. RVVATG now supports user-defined configurations to dynamically generate the coverages that RISC-V foundation specifies.

### External Dependencies

1. `gcc`, the customized version that supports V-Extention, compiles the assembly tests.
2. `sail`, the customized version that supports V-Extention, runs the instructions flow compiled by gcc.
3. `riscv-isac`, helps to generate coverage reports.
4. `spike`, serves as a reference model against the customized sail.

### Features
#### Configuration Support
|Parameter|        Numbers        | Current Support | Note               |
|-----|---------------------------|---------------- |--------------------|
|vlen | 128~2^16                  | 128 ~ 2^16      | `vlen > 4096` is currently under coverage verification|
|sew  | 8, 16, 32, 64             | ALL             |                    |
|lmul | 1/8, 1/4, 1/2, 1, 2, 4, 8 | ALL             |                    |
|vta  | 0, 1                      | 0               | `vta = 1` can be extended if needed |
|vma  | 0, 1                      | 0               | `vma = 1` can be extended if needed |
#### Instruction Coverage
RVVATG supports the complete set of currently frozen vector instructions. The instructions are divided into six types, 1) mask type like `vmand`, 2) permute type like `vcompress`, 3) integer type like `vadd`, 4) floating points type like `vfadd`, 5) fixed points type like `vaadd`, and 6) load and store types like `vle32` and `vse32`, while all the instructions follow the same generating flow above.

Spike now does not support all the instructions, for example, `vfcvt` and `vfncvt` cannot be recognized by the current spike. And that is why we use [customized sail](#based-on-the-customized-sail-that-supports-v-extension). 

#### Source Reg & Value Coverage
For some reason, covering all the registers and all the source values will be impossible. For example, all the registers can not be covered when `vlen = 4096` and all the source values can be covered when divided by `zero`. So, RVVATG supports user-defined registers and source value coverages. See [Appendix B](#appendix-b-register-value-combinations) for the detailed source value combinations.

#### Configuration Coverage
For every vector instruction, different parameters should be combined and covered. Now RVVATG supports `vlen` from 128 to 4096, and all the `sew` as well as `lmul` numbers. For example, `vlen = 128`, `sew = 32`, and `lmul = 1`. The table below shows the supporting status of the RVVATG.
| instruction type | vlen    | sew       | lmul | status |
| ---------------- | --------  | ------------  | ------------------------- | ---------------- |
| Mask             |128 ~ 4096 | 8, 16, 32, 64 | 1/8, 1/4, 1/2, 1, 2, 4, 8 |:white_check_mark:|
| Permute          |128 ~ 4096 | 8, 16, 32, 64 | 1/8, 1/4, 1/2, 1, 2, 4, 8 |:white_check_mark:|
| Integer          |128 ~ 4096 | 8, 16, 32, 64 | 1/8, 1/4, 1/2, 1, 2, 4, 8 |:white_check_mark:|
| Floating Points  |128 ~ 4096 | 8, 16, 32, 64 | 1/8, 1/4, 1/2, 1, 2, 4, 8 |:white_check_mark:|
| Fixed Points     |128 ~ 4096 | 8, 16, 32, 64 | 1/8, 1/4, 1/2, 1, 2, 4, 8 |:white_check_mark:|
| Load/Store       |128 ~ 4096 | 8, 16, 32, 64 | 1/8, 1/4, 1/2, 1, 2, 4, 8 |:white_check_mark:|

> **Note:**
> See [Appendix A](#appendix-a) for the detail supporting status of the combination between instructions and parameters.

#### Extended RISCV-ISAC With Vector Extension
RISC-V ISAC is an ISA coverage extraction tool. Given a set of cover points and an execution trace of a test/application run on a model, ISAC can provide a report indicating in detail which of those cover points were covered by the test/application. ISAC also holds the capability to provide a detailed qualitative analysis of data propagation occurring within the test/application. The figure below shows the overall flow of RISCV-ISAC.
![](images/riscv-isac.png)

ISAC-RVV is designed as RISC-V Vector Extension(RVV) support for RISCV-ISAC.

#### RIOS RVV Sail Model 
![](images/sail.png)
Sail is a new Domain-Specific Language (DSL) to build a model that accurately describes the behavior of the instructions and related contents of a given ISA. And the Sail model is used as the golden reference model of modern computer architectures ISA design.

Sail, as a golden function model, is designed to guarantee the correctness of execution. And Sail-RVV, as a machine-readable golden reference "V" extension model, is designed to cover the full scope of the architecture and offer fine-grained execution information for relaxed-memory model integration as well as provide the architectural tests for RVV.
## Quickstart
### Deploy and Run the RVVATG
RVVATG depends on the customized RVV compiler like gcc, the customized sail, and the specific RISCV-ISAC.
1. Specify RVV compiler
   Set `gcc`, `objdump`, and `riscof` directory's path variables.
2. Download specific [RISCV-ISAC](https://github.com/riscv-software-src/riscv-isac).
3. Install the latest [spike](https://github.com/riscv-software-src/riscv-isa-sim).
4. Sample to generate one instruction
   From terminal run: `python run.py -i <instruction> -t <type> [--vlen VLEN] [--sew SEW]`]
   > **Note:**
   > 1. The type shall be consistent with the instruction: i (integer), f (floating point), m (mask), p (permute), x (fix point), l (load/store).
   > 2. Supported instruction and type can be seen in `cgfs/<type>/<instruction>.yaml`
   > 3. VLEN Vector Register Length: 32, 64, 128(default), 256, 512, 1024, 2048, and 4096.
   > 4. VSEW Selected Element Width: 8, 16, 32(default), 64.
   > 5. LMUL Vector register group multiplier: 1/8, 1/4, 1/2, 1, 2, 4, and 8.
   > 6. Other options can be seen through `python run.py -h`
5. Sample to generate all instructions
    From terminal run: `python generate_all.py`
    > **Note:**
    > 1. It will use default parameter configuration to generate all integer instructions tests.

## Appendix A
### Mask

#### vmand, vmandnot, vmnand, vmor, vmornot, vmxnor, vmxor; vmsbf; vpopc, vfirst; vid, viota

| Config                        | Status | Config                  | Status | Config                        | Status |
| ----------------------------- | ------ | ---------------------   | ------ | ---------------------         | ------ |
| vlen128 vsew8 lmul1           |:white_check_mark:| vlen128 vsew8   lmul2-8 |:white_check_mark:| vlen128 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen128 vsew16 lmul1          |:white_check_mark:| vlen128 vsew16  lmul2-8 |:white_check_mark:| vlen128 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen128 vsew32 lmul1(default) |:white_check_mark:| vlen128 vsew32  lmul2-8 |:white_check_mark:| vlen128 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen128 vsew64 lmul1          |:white_check_mark:| vlen128 vsew64  lmul2-8 |:white_check_mark:| vlen128 vsew64  lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew8 lmul1           |:white_check_mark:| vlen256 vsew8   lmul2-8 |:white_check_mark:| vlen256 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew16 lmul1          |:white_check_mark:| vlen256 vsew16  lmul2-8 |:white_check_mark:| vlen256 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew32 lmul1          |:white_check_mark:| vlen256 vsew32  lmul2-8 |:white_check_mark:| vlen256 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew64 lmul1          |:white_check_mark:| vlen256 vsew64  lmul2-8 |:white_check_mark:| vlen256 vsew64  lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew8 lmul1           |:white_check_mark:| vlen512 vsew8   lmul2-8 |:white_check_mark:| vlen512 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew16 lmul1          |:white_check_mark:| vlen512 vsew16  lmul2-8 |:white_check_mark:| vlen512 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew32 lmul1          |:white_check_mark:| vlen512 vsew32  lmul2-8 |:white_check_mark:| vlen512 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew64 lmul1          |:white_check_mark:| vlen512 vsew64  lmul2-8 |:white_check_mark:| vlen512 vsew64  lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew8 lmul1          |:white_check_mark:| vlen1024-4096 vsew8  lmul2-8 |:white_check_mark:| vlen1024-4096 vsew8  lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew16 lmul1         |:white_check_mark:| vlen1024-4096 vsew16 lmul2-8 |:white_check_mark:| vlen1024-4096 vsew16 lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew32 lmul1         |:white_check_mark:| vlen1024-4096 vsew32 lmul2-8 |:white_check_mark:| vlen1024-4096 vsew32 lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew64 lmul1         |:white_check_mark:| vlen1024-4096 vsew64 lmul2-8 |:white_check_mark:| vlen1024-4096 vsew64 lmul0.125-0.5 |:white_check_mark:|

### Permute
⚠️ vslide, vcompress will generate too many tests if num_elems (vlen*lmul/sew) is too big. Test them will take a long time (longer than 20min)
#### vcompress, vmre, vmv, vfmv, vrgather, vrgatherei16, vslide, vslide1, vfslide

| Config                            | vcompress | vmre | vmv | vfmv | vrgather | vrgatherei16 | vslide | vslide1 | vfslide |
| -----------------------------     | --------- | ---- | --- | ---- | -------- | ------------ | ------ | ------- | ------- |
| vlen128-4096 vsew8  lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-4096 vsew16 lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-4096 vsew32 lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-4096 vsew64 lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-4096 vsew8  lmul0.125-0.5 |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-4096 vsew16 lmul0.125-0.5 |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-4096 vsew32 lmul0.125-0.5 |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-4096 vsew64 lmul0.125-0.5 |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
> **Note:**
>  For `vmv` instruction, there are two requirements:
>   - vsew32 requires rs1val_walking_vector_unsgn
>   - vsew64 requires rs1val_walking_vector

### Integer

#### Simple Arithmetic: vadc, vsbc; vadd, vand, vdiv, vdivu,  vmul, vmulh, vmulhsu, vmulhu, vsll, vsra, vsrl, vsub, vxor, vrem, vremu, vrsub, vsadd, vsaddu, vssub, vssubu, vmax, vmaxu, vmin, vminu,

| Config                        | Status | Config                  | Status | Config                        | Status |
| ----------------------------- | ------ | ----------------------  | ------ | ---------------------         | ------ |
| vlen128 vsew8 lmul1           |:white_check_mark:| vlen128 vsew8   lmul2-8 |:white_check_mark:| vlen128 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen128 vsew16 lmul1          |:white_check_mark:| vlen128 vsew16  lmul2-8 |:white_check_mark:| vlen128 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen128 vsew32 lmul1(default) |:white_check_mark:| vlen128 vsew32  lmul2-8 |:white_check_mark:| vlen128 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen128 vsew64 lmul1          |:white_check_mark:| vlen128 vsew64  lmul2-8 |:white_check_mark:| vlen128 vsew64  lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew8 lmul1           |:white_check_mark:| vlen256 vsew8   lmul2-8 |:white_check_mark:| vlen256 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew16 lmul1          |:white_check_mark:| vlen256 vsew16  lmul2-8 |:white_check_mark:| vlen256 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew32 lmul1          |:white_check_mark:| vlen256 vsew32  lmul2-8 |:white_check_mark:| vlen256 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew64 lmul1          |:white_check_mark:| vlen256 vsew64  lmul2-8 |:white_check_mark:| vlen256 vsew64  lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew8 lmul1           |:white_check_mark:| vlen512 vsew8   lmul2-8 |:white_check_mark:| vlen512 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew16 lmul1          |:white_check_mark:| vlen512 vsew16  lmul2-8 |:white_check_mark:| vlen512 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew32 lmul1          |:white_check_mark:| vlen512 vsew32  lmul2-8 |:white_check_mark:| vlen512 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew64 lmul1          |:white_check_mark:| vlen512 vsew64  lmul2-8 |:white_check_mark:| vlen512 vsew64  lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew8 lmul1          |:white_check_mark:| vlen1024-4096 vsew8  lmul2-8 |:white_check_mark:| vlen1024-4096 vsew8  lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew16 lmul1         |:white_check_mark:| vlen1024-4096 vsew16 lmul2-8 |:white_check_mark:| vlen1024-4096 vsew16 lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew32 lmul1         |:white_check_mark:| vlen1024-4096 vsew32 lmuL2-8 |:white_check_mark:| vlen1024-4096 vsew32 lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew64 lmul1         |:white_check_mark:| vlen1024-4096 vsew64 lmul2-8 |:white_check_mark:| vlen1024-4096 vsew64 lmul0.125-0.5 |:white_check_mark:|

#### Multiply-Add & Add-with-Carry Subtract-with-Borrow & Comparison: vmacc, vmadd, vnmsac, vnmsub & vmadc, vmsbc & vmseq, vmsgt, vmsgtu, vmsle, vmsleu, vmslt, vmsltu, vmsne

| Config                        | Status | Config                 | Status | Config                        | Status |
| ----------------------------- | ------ | --------------------   | ------ | ---------------------         | ------ |
| vlen128 vsew8 lmul1           |:white_check_mark:| vlen128 vsew8   lmul2-8|:white_check_mark:| vlen128 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen128 vsew16 lmul1          |:white_check_mark:| vlen128 vsew16  lmul2-8|:white_check_mark:| vlen128 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen128 vsew32 lmul1(default) |:white_check_mark:| vlen128 vsew32  lmul2-8|:white_check_mark:| vlen128 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen128 vsew64 lmul1          |:white_check_mark:| vlen128 vsew64  lmul2-8|:white_check_mark:| vlen128 vsew64  lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew8 lmul1           |:white_check_mark:| vlen256 vsew8   lmul2-8|:white_check_mark:| vlen256 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew16 lmul1          |:white_check_mark:| vlen256 vsew16  lmul2-8|:white_check_mark:| vlen256 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew32 lmul1          |:white_check_mark:| vlen256 vsew32  lmul2-8|:white_check_mark:| vlen256 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew64 lmul1          |:white_check_mark:| vlen256 vsew64  lmul2-8|:white_check_mark:| vlen256 vsew64  lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew8 lmul1           |:white_check_mark:| vlen512 vsew8   lmul2-8|:white_check_mark:| vlen512 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew16 lmul1          |:white_check_mark:| vlen512 vsew16  lmul2-8|:white_check_mark:| vlen512 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew32 lmul1          |:white_check_mark:| vlen512 vsew32  lmul2-8|:white_check_mark:| vlen512 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew64 lmul1          |:white_check_mark:| vlen512 vsew64  lmul2-8|:white_check_mark:| vlen512 vsew64  lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew8 lmul1          |:white_check_mark:| vlen1024-4096 vsew8  lmul2-8|:white_check_mark:| vlen1024-4096 vsew8  lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew16 lmul1         |:white_check_mark:| vlen1024-4096 vsew16 lmul2-8|:white_check_mark:| vlen1024-4096 vsew16 lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew32 lmul1         |:white_check_mark:| vlen1024-4096 vsew32 lmul2-8|:white_check_mark:| vlen1024-4096 vsew32 lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew64 lmul1         |:white_check_mark:| vlen1024-4096 vsew64 lmul2-8|:white_check_mark:| vlen1024-4096 vsew64 lmul0.125-0.5 |:white_check_mark:|

#### Reduction Arithmetic:  vor; vredand, vredmax, vredmaxu, vredmin, vredminu, vredor, vredsum, vredxor;

| Config                        | Status | Config                 | Status | Config                        | Status |
| ----------------------------- | ------ | --------------------   | ------ | ---------------------         | ------ |
| vlen128 vsew8 lmul1           |:white_check_mark:| vlen128 vsew8   lmul2-8|:white_check_mark:| vlen128 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen128 vsew16 lmul1          |:white_check_mark:| vlen128 vsew16  lmul2-8|:white_check_mark:| vlen128 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen128 vsew32 lmul1(default) |:white_check_mark:| vlen128 vsew32  lmul2-8|:white_check_mark:| vlen128 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen128 vsew64 lmul1          |:white_check_mark:| vlen128 vsew64  lmul2-8|:white_check_mark:| vlen128 vsew64  lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew8 lmul1           |:white_check_mark:| vlen256 vsew8   lmul2-8|:white_check_mark:| vlen256 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew16 lmul1          |:white_check_mark:| vlen256 vsew16  lmul2-8|:white_check_mark:| vlen256 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew32 lmul1          |:white_check_mark:| vlen256 vsew32  lmul2-8|:white_check_mark:| vlen256 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew64 lmul1          |:white_check_mark:| vlen256 vsew64  lmul2-8|:white_check_mark:| vlen256 vsew64  lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew8 lmul1           |:white_check_mark:| vlen512 vsew8   lmul2-8|:white_check_mark:| vlen512 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew16 lmul1          |:white_check_mark:| vlen512 vsew16  lmul2-8|:white_check_mark:| vlen512 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew32 lmul1          |:white_check_mark:| vlen512 vsew32  lmul2-8|:white_check_mark:| vlen512 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew64 lmul1          |:white_check_mark:| vlen512 vsew64  lmul2-8|:white_check_mark:| vlen512 vsew64  lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew8 lmul1          |:white_check_mark:| vlen1024-4096 vsew8  lmul2-8|:white_check_mark:| vlen1024-4096 vsew8  lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew16 lmul1         |:white_check_mark:| vlen1024-4096 vsew16 lmul2-8|:white_check_mark:| vlen1024-4096 vsew16 lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew32 lmul1         |:white_check_mark:| vlen1024-4096 vsew32 lmul2-8|:white_check_mark:| vlen1024-4096 vsew32 lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew64 lmul1         |:white_check_mark:| vlen1024-4096 vsew64 lmul2-8|:white_check_mark:| vlen1024-4096 vsew64 lmul0.125-0.5 |:white_check_mark:|

#### Widen Arithmetic: vwadd, vwaddu, vwmacc, vwmaccsu, vwmaccu, vwmaccus; vwmul, vwmulsu, vwmulu, vwredsum, vwredsumu, vwsub, vwsubu; vnsra, vnsrl;

* widen and narrow instructions do not support `VSEW=64` (sew should be `<= 64`)
* widen instructions(except VWRED*) do not support `LMUL=8` (lmul should `be <= 4`), `lmul8` in table only means `VWRED* pass`

| Config                        | Status | Config                 | Status | Config                        | Status |
| ----------------------------- | ------ | --------------------   | ------ | --------------------          | ------ |
| vlen128 vsew8 lmul1           |:white_check_mark:| vlen128 vsew8   lmul2-8|:white_check_mark:| vlen128 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen128 vsew16 lmul1          |:white_check_mark:| vlen128 vsew16  lmul2-8|:white_check_mark:| vlen128 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen128 vsew32 lmul1(default) |:white_check_mark:| vlen128 vsew32  lmul2-8|:white_check_mark:| vlen128 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew8 lmul1           |:white_check_mark:| vlen256 vsew8   lmul2-8|:white_check_mark:| vlen256 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew16 lmul1          |:white_check_mark:| vlen256 vsew16  lmul2-8|:white_check_mark:| vlen256 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew32 lmul1          |:white_check_mark:| vlen256 vsew32  lmul2-8|:white_check_mark:| vlen256 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew8 lmul1           |:white_check_mark:| vlen512 vsew8   lmul2-8|:white_check_mark:| vlen512 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew16 lmul1          |:white_check_mark:| vlen512 vsew16  lmul2-8|:white_check_mark:| vlen512 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew32 lmul1          |:white_check_mark:| vlen512 vsew32  lmul2-8|:white_check_mark:| vlen512 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew8 lmul1          |:white_check_mark:| vlen1024-4096 vsew8  lmul2-8|:white_check_mark:| vlen1024-4096 vsew8  lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew16 lmul1         |:white_check_mark:| vlen1024-4096 vsew16 lmul2-8|:white_check_mark:| vlen1024-4096 vsew16 lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew32 lmul1         |:white_check_mark:| vlen1024-4096 vsew32 lmul2-8|:white_check_mark:| vlen1024-4096 vsew32 lmul0.125-0.5 |:white_check_mark:|

### Floating Points

#### vfadd, vfclass, `vfcvt`, vfdiv, vfmacc, vfmax, vfmin, vfmsac, vfmsub, vfmul, `vfncvt`, vfnmacc, vfnmadd, vfnmsac, vfnmsub, vfrdiv, vfrec7, vfredmax, vfredmin
| Config                        | Status | Config                 | Status | Config                        | Status |
| ----------------------------- | ------ | --------------------   | ------ | --------------------          | ------ |
| vlen128 vsew8 lmul1           |:white_check_mark:| vlen128 vsew8   lmul2-8|:white_check_mark:| vlen128 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen128 vsew16 lmul1          |:white_check_mark:| vlen128 vsew16  lmul2-8|:white_check_mark:| vlen128 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen128 vsew32 lmul1(default) |:white_check_mark:| vlen128 vsew32  lmul2-8|:white_check_mark:| vlen128 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew8 lmul1           |:white_check_mark:| vlen256 vsew8   lmul2-8|:white_check_mark:| vlen256 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew16 lmul1          |:white_check_mark:| vlen256 vsew16  lmul2-8|:white_check_mark:| vlen256 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen256 vsew32 lmul1          |:white_check_mark:| vlen256 vsew32  lmul2-8|:white_check_mark:| vlen256 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew8 lmul1           |:white_check_mark:| vlen512 vsew8   lmul2-8|:white_check_mark:| vlen512 vsew8   lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew16 lmul1          |:white_check_mark:| vlen512 vsew16  lmul2-8|:white_check_mark:| vlen512 vsew16  lmul0.125-0.5 |:white_check_mark:|
| vlen512 vsew32 lmul1          |:white_check_mark:| vlen512 vsew32  lmul2-8|:white_check_mark:| vlen512 vsew32  lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew8 lmul1          |:white_check_mark:| vlen1024-4096 vsew8  lmul2-8|:white_check_mark:| vlen1024-4096 vsew8  lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew16 lmul1         |:white_check_mark:| vlen1024-4096 vsew16 lmul2-8|:white_check_mark:| vlen1024-4096 vsew16 lmul0.125-0.5 |:white_check_mark:|
| vlen1024-4096 vsew32 lmul1         |:white_check_mark:| vlen1024-4096 vsew32 lmul2-8|:white_check_mark:| vlen1024-4096 vsew32 lmul0.125-0.5 |:white_check_mark:|

> **Note:**
> `vfcvt`, `vfcvt.x.f.v`, `vfcvt.rtz.xu.f.v`, etc. have not been supported in Spike.

#### vfredosum, vfredusum, vfrsqrt7, vfrsub, vfsgnj, vfsgnjn, vfsgnjx, vfsqrt, vfsub

| Config                            | vfredosum | vfredusum | vfrsqrt7 | vfrsub | vfsgnj | vfsgnjn | vfsgnjx | vfsqrt | vfsub |
| -----------------------------     | --------- | --------- | -------- | ------ | ------ | ------- | ------- | ------ | ----- |
| vlen128 vsew32 lmul1(default)     |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128 vsew64 lmul1              |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen256 vsew32 lmul1              |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen256 vsew64 lmul1              |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen512 vsew32 lmul1              |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen512 vsew64 lmul1              |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen1024-4096 vsew32 lmul1             |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen1024-4096 vsew64 lmul1             |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew32 lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew64 lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen1024-4096 vsew32 lmul0.125-8  |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen1024-4096 vsew64 lmul0.125-8  |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|

#### vfwadd, vfwcvt, vfwmacc, vfwmsac, vfwmul, vfwnmacc, vfwnmsac, vfwredsum, vfwsub

| Config                        | vfwadd | vfwcvt | vfwmacc | vfwmsac | vfwmul | vfwnmacc | vfwnmsac | vfwredsum | vfwsub |
| ----------------------------- | ------ | ------ | ------- | ------- | ------ | -------- | -------- | --------- | ------ |
| vlen128 vsew16 lmul1          |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128 vsew32 lmul1(default) |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen256 vsew16 lmul1          |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen256 vsew32 lmul1          |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen512 vsew16 lmul1          |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen512 vsew32 lmul1          |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen1024-4096 vsew16 lmul1         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen1024-4096 vsew32 lmul1         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128 vsew16  lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128 vsew32  lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen256 vsew16  lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen256 vsew32  lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen512 vsew16  lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen512 vsew32  lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen1024-4096 vsew16 lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen1024-4096 vsew32 lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128 vsew16  lmul0.125-0.5 |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128 vsew32  lmul0.125-0.5 |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen256 vsew16  lmul0.125-0.5 |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen256 vsew32  lmul0.125-0.5 |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen512 vsew16  lmul0.125-0.5 |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen512 vsew32  lmul0.125-0.5 |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen1024-4096 vsew16 lmul0.125-0.5 |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen1024-4096 vsew32 lmul0.125-0.5 |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
> **Note:**
> - We do not have 16-bit floating point dataset
> - VFWiden instructions require lmul <= 4

### Fix Points

|   Config                          | vaadd | vaaddu | vasub | vasubu | vnclip | vnclipu | vsmul | vssra | vssrl |
| -----------------------------     | ----- | ------ | ----- | ------ | ------ | ------- | ----- | ----- | ----- |
| vlen128 vsew8 lmul1               |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128 vsew16 lmul1              |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128 vsew32 lmul1(default)     |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128 vsew64 lmul1              |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen256 vsew8 lmul1               |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen256 vsew16 lmul1              |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen256 vsew32 lmul1              |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen256 vsew64 lmul1              |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen512 vsew8 lmul1               |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen512 vsew16 lmul1              |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen512 vsew32 lmul1              |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen512 vsew64 lmul1              |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen1024-4096 vsew8 lmul1              |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen1024-4096 vsew16 lmul1             |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen1024-4096 vsew32 lmul1             |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen1024-4096 vsew64 lmul1             |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew8  lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew16 lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew32 lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew64 lmul2-8       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew8  lmul0.125-0.5 |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew16 lmul0.125-0.5 |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew32 lmul0.125-0.5 |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew64 lmul0.125-0.5 |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|

### Load

- Unable to pass the lmul==8 parameter test： vlsege32 vlssege32 vluxsegei8 vluxsegei16 vluxsegei32
- If the EMUL would be out of range (EMUL>8 or EMUL<1/8), the instruction encoding is reserved. Use `/` to represent.
- `except <eew>` means this configuration not satisfy `vemul >= 0.125 && vemul <= 8` or `sew <= elen * lmul`

|       Config                      | vle8/16/32/64 | vlre8/16/32 | vlse8/16/32/64 | vls(s)ege8/16/32 | vluxei8/16/32 | vluxeiseg8/16/32 | note                                |
| -----------------------------     | ------------- | ----------- | -----------    | ---------------- | ------------- | ---------------- | ----                                |
| vlen128-1024 vsew8 lmul1          |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew16 lmul1         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew32 lmul1(default)|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew64 lmul1         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew8  lmul0.125     |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:| vle, vlse, vls(s)ege, vlre except32; vle, vlse except 64;
| vlen128-1024 vsew16 lmul0.125     |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:| vle, vlse, vls(s)ege, vlre except32; vle, vlse except 64; vluxseg except 8
| vlen128-1024 vsew32 lmul0.125     |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew64 lmul0.125     |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew8  lmul0.25      |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:| vle,vlse except 64
| vlen128-1024 vsew16 lmul0.25      |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:| vle,vlse except 64
| vlen128-1024 vsew32 lmul0.25      |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:| vle,vlse except 64; vluxseg except 8
| vlen128-1024 vsew64 lmul0.25      |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew8  lmul0.5       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew16 lmul0.5       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew32 lmul0.5       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew64 lmul0.5       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:| vluxseg except 8
| vlen128-1024 vsew8  lmul2         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew16 lmul2         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew32 lmul2         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew64 lmul2         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew8  lmul4         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew16 lmul4         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew32 lmul4         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew64 lmul4         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew8  lmul8         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew16 lmul8         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew32 lmul8         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew64 lmul8         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|

### Store

|       Config                      | vse8/16/32/64 | vs1/2/4/8r  | vsse8/16/32    | vss(s)ege8/16/32 | vsuxei8/16/32 | vsuxeiseg8/16/32 | note     |
| ------------------------------    | ------------- | ----------  | -----------    | ---------------- | ------------- | ---------------- | -----    |
| vlen128-1024 vsew8 lmul1          |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew16 lmul1         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew32 lmul1(default)|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew64 lmul1         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew8  lmul0.125     |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:| vse, vss(s)ege except32;
| vlen128-1024 vsew16 lmul0.125     |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:| vse, vss(s)ege except32;
| vlen128-1024 vsew32 lmul0.125     |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew64 lmul0.125     |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew8  lmul0.25      |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew16 lmul0.25      |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew32 lmul0.25      |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew64 lmul0.25      |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:| 
| vlen128-1024 vsew8  lmul0.5       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew16 lmul0.5       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew32 lmul0.5       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew64 lmul0.5       |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew8  lmul2         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew16 lmul2         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew32 lmul2         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew64 lmul2         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew8  lmul4         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew16 lmul4         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew32 lmul4         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew64 lmul4         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew8  lmul8         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew16 lmul8         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew32 lmul8         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
| vlen128-1024 vsew64 lmul8         |:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|

## Appendix B: Register value combinations.
The table below shows the detailed register values that should be covered.

### Instruction type: Integer/Floating Points/Fixed Points
| No. | rs1    | rs2       | status | note |
| --- | ------ | --------- | ------ | ---- |
| 1 |  0x00000000 |  0x00000000  |:white_check_mark:| `sew == 32` |
| 2 |  0xBF800000 |  0xBF800000  |:white_check_mark:| `sew == 32` |
| 3 |  0xBF800000 |  0x3F800000  |:white_check_mark:| `sew == 32` |
| 4 |  0xBF800000 |  0xFF7FFFFF |:white_check_mark:| `sew == 32` |
| 5 |  0xBF800000 |  0x7F7FFFFF |:white_check_mark:| `sew == 32` |
| 6 |  0xBF800000 |  0x80855555 |:white_check_mark:| `sew == 32` |
| 7 |  0xBF800000 |  0x00800001 |:white_check_mark:| `sew == 32` |
| 8 |  0xBF800000 |  0x80800000 |:white_check_mark:| `sew == 32` |
| 9 |  0xBF800000 |  0x00800000 |:white_check_mark:| `sew == 32` |
| 10 |  0xBF800000 |  0x807FFFFF |:white_check_mark:| `sew == 32` |
| 11 |  0xBF800000 |  0x007FFFFF |:white_check_mark:| `sew == 32` |
| 12 |  0xBF800000 |  0x807FFFFE |:white_check_mark:| `sew == 32` |
| 13 |  0xBF800000 |  0x00000002 |:white_check_mark:| `sew == 32` |
| 14 |  0xBF800000 |  0x80000001 |:white_check_mark:| `sew == 32` |
| 15 |  0xBF800000 |  0x00000001 |:white_check_mark:| `sew == 32` |
| 16 |  0xBF800000 |  0x80000000 |:white_check_mark:| `sew == 32` |
| 17 |  0xBF800000 |  0x00000000 |:white_check_mark:| `sew == 32` |
| 18 |  0x3F800000 |  0xBF800000 |:white_check_mark:| `sew == 32` |
| 19 |  0x3F800000 |  0x3F800000 |:white_check_mark:| `sew == 32` |
| 20 |  0x3F800000 |  0xFF7FFFFF |:white_check_mark:| `sew == 32` |
| 21 |  0x3F800000 |  0x7F7FFFFF |:white_check_mark:| `sew == 32` |
| 22 |  0x3F800000 |  0x80855555 |:white_check_mark:| `sew == 32` |
| 23 |  0x3F800000 |  0x00800001 |:white_check_mark:| `sew == 32` |
| 24 |  0x3F800000 |  0x80800000 |:white_check_mark:| `sew == 32` |
| 25 |  0x3F800000 |  0x00800000 |:white_check_mark:| `sew == 32` |
| 26 |  0x3F800000 |  0x807FFFFF |:white_check_mark:| `sew == 32` |
| 27 |  0x3F800000 |  0x007FFFFF |:white_check_mark:| `sew == 32` |
| 28 |  0x3F800000 |  0x807FFFFE |:white_check_mark:| `sew == 32` |
| 29 |  0x3F800000 |  0x00000002 |:white_check_mark:| `sew == 32` |
| 30 |  0x3F800000 |  0x80000001 |:white_check_mark:| `sew == 32` |
| 31 |  0x3F800000 |  0x00000001 |:white_check_mark:| `sew == 32` |
| 32 |  0x3F800000 |  0x80000000 |:white_check_mark:| `sew == 32` |
| 33 |  0x3F800000 |  0x00000000 |:white_check_mark:| `sew == 32` |
| 34 |  0xFF7FFFFF |  0xBF800000 |:white_check_mark:| `sew == 32` |
| 35 |  0xFF7FFFFF |  0x3F800000 |:white_check_mark:| `sew == 32` |
| 36 |  0xFF7FFFFF |  0xFF7FFFFF |:white_check_mark:| `sew == 32` |
| 37 |  0xFF7FFFFF |  0x7F7FFFFF |:white_check_mark:| `sew == 32` |
| 38 |  0xFF7FFFFF |  0x80855555 |:white_check_mark:| `sew == 32` |
| 39 |  0xFF7FFFFF |  0x00800001 |:white_check_mark:| `sew == 32` |
| 40 |  0xFF7FFFFF |  0x80800000 |:white_check_mark:| `sew == 32` |
| 41 |  0xFF7FFFFF |  0x00800000 |:white_check_mark:| `sew == 32` |
| 42 |  0xFF7FFFFF |  0x807FFFFF |:white_check_mark:| `sew == 32` |
| 43 |  0xFF7FFFFF |  0x007FFFFF |:white_check_mark:| `sew == 32` |
| 44 |  0xFF7FFFFF |  0x807FFFFE |:white_check_mark:| `sew == 32` |
| 45 |  0xFF7FFFFF |  0x00000002 |:white_check_mark:| `sew == 32` |
| 46 |  0xFF7FFFFF |  0x80000001 |:white_check_mark:| `sew == 32` |
| 47 |  0xFF7FFFFF |  0x00000001 |:white_check_mark:| `sew == 32` |
| 48 |  0xFF7FFFFF |  0x80000000 |:white_check_mark:| `sew == 32` |
| 49 |  0xFF7FFFFF |  0x00000000 |:white_check_mark:| `sew == 32` |
| 50 |  0x7F7FFFFF |  0xBF800000 |:white_check_mark:| `sew == 32` |
| 51 |  0x7F7FFFFF |  0x3F800000 |:white_check_mark:| `sew == 32` |
| 52 |  0x7F7FFFFF |  0xFF7FFFFF |:white_check_mark:| `sew == 32` |
| 53 |  0x7F7FFFFF |  0x7F7FFFFF |:white_check_mark:| `sew == 32` |
| 54 |  0x7F7FFFFF |  0x80855555 |:white_check_mark:| `sew == 32` |
| 55 |  0x7F7FFFFF |  0x00800001 |:white_check_mark:| `sew == 32` |
| 56 |  0x7F7FFFFF |  0x80800000 |:white_check_mark:| `sew == 32` |
| 57 |  0x7F7FFFFF |  0x00800000 |:white_check_mark:| `sew == 32` |
| 58 |  0x7F7FFFFF |  0x807FFFFF |:white_check_mark:| `sew == 32` |
| 59 |  0x7F7FFFFF |  0x007FFFFF |:white_check_mark:| `sew == 32` |
| 60 |  0x7F7FFFFF |  0x807FFFFE |:white_check_mark:| `sew == 32` |
| 61 |  0x7F7FFFFF |  0x00000002 |:white_check_mark:| `sew == 32` |
| 62 |  0x7F7FFFFF |  0x80000001 |:white_check_mark:| `sew == 32` |
| 63 |  0x7F7FFFFF |  0x00000001 |:white_check_mark:| `sew == 32` |
| 64 |  0x7F7FFFFF |  0x80000000 |:white_check_mark:| `sew == 32` |
| 65 |  0x7F7FFFFF |  0x00000000 |:white_check_mark:| `sew == 32` |
| 66 |  0x80855555 |  0xBF800000 |:white_check_mark:| `sew == 32` |
| 67 |  0x80855555 |  0x3F800000 |:white_check_mark:| `sew == 32` |
| 68 |  0x80855555 |  0xFF7FFFFF |:white_check_mark:| `sew == 32` |
| 69 |  0x80855555 |  0x7F7FFFFF |:white_check_mark:| `sew == 32` |
| 70 |  0x80855555 |  0x80855555 |:white_check_mark:| `sew == 32` |
| 71 |  0x80855555 |  0x00800001 |:white_check_mark:| `sew == 32` |
| 72 |  0x80855555 |  0x80800000 |:white_check_mark:| `sew == 32` |
| 73 |  0x80855555 |  0x00800000 |:white_check_mark:| `sew == 32` |
| 74 |  0x80855555 |  0x807FFFFF |:white_check_mark:| `sew == 32` |
| 75 |  0x80855555 |  0x007FFFFF |:white_check_mark:| `sew == 32` |
| 76 |  0x80855555 |  0x807FFFFE |:white_check_mark:| `sew == 32` |
| 77 |  0x80855555 |  0x00000002 |:white_check_mark:| `sew == 32` |
| 78 |  0x80855555 |  0x80000001 |:white_check_mark:| `sew == 32` |
| 79 |  0x80855555 |  0x00000001 |:white_check_mark:| `sew == 32` |
| 80 |  0x80855555 |  0x80000000 |:white_check_mark:| `sew == 32` |
| 81 |  0x80855555 |  0x00000000 |:white_check_mark:| `sew == 32` |
| 82 |  0x00800001 |  0xBF800000 |:white_check_mark:| `sew == 32` |
| 83 |  0x00800001 |  0x3F800000 |:white_check_mark:| `sew == 32` |
| 84 |  0x00800001 |  0xFF7FFFFF |:white_check_mark:| `sew == 32` |
| 85 |  0x00800001 |  0x7F7FFFFF |:white_check_mark:| `sew == 32` |
| 86 |  0x00800001 |  0x80855555 |:white_check_mark:| `sew == 32` |
| 87 |  0x00800001 |  0x00800001 |:white_check_mark:| `sew == 32` |
| 88 |  0x00800001 |  0x80800000 |:white_check_mark:| `sew == 32` |
| 89 |  0x00800001 |  0x00800000 |:white_check_mark:| `sew == 32` |
| 90 |  0x00800001 |  0x807FFFFF |:white_check_mark:| `sew == 32` |
| 91 |  0x00800001 |  0x007FFFFF |:white_check_mark:| `sew == 32` |
| 92 |  0x00800001 |  0x807FFFFE |:white_check_mark:| `sew == 32` |
| 93 |  0x00800001 |  0x00000002 |:white_check_mark:| `sew == 32` |
| 94 |  0x00800001 |  0x80000001 |:white_check_mark:| `sew == 32` |
| 95 |  0x00800001 |  0x00000001 |:white_check_mark:| `sew == 32` |
| 96 |  0x00800001 |  0x80000000 |:white_check_mark:| `sew == 32` |
| 97 |  0x00800001 |  0x00000000 |:white_check_mark:| `sew == 32` |
| 98 |  0x80800000 |  0xBF800000 |:white_check_mark:| `sew == 32` |
| 99 |  0x80800000 |  0x3F800000 |:white_check_mark:| `sew == 32` |
| 100  | 0x80800000 | 0xFF7FFFFF |:white_check_mark:| `sew == 32` |
| 101  | 0x80800000 | 0x7F7FFFFF |:white_check_mark:| `sew == 32` |
| 102  | 0x80800000 | 0x80855555 |:white_check_mark:| `sew == 32` |
| 103  | 0x80800000 | 0x00800001 |:white_check_mark:| `sew == 32` |
| 104  | 0x80800000 | 0x80800000 |:white_check_mark:| `sew == 32` |
| 105  | 0x80800000 | 0x00800000 |:white_check_mark:| `sew == 32` |
| 106  | 0x80800000 | 0x807FFFFF |:white_check_mark:| `sew == 32` |
| 107  | 0x80800000 | 0x007FFFFF |:white_check_mark:| `sew == 32` |
| 108  | 0x80800000 | 0x807FFFFE |:white_check_mark:| `sew == 32` |
| 109  | 0x80800000 | 0x00000002 |:white_check_mark:| `sew == 32` |
| 110  | 0x80800000 | 0x80000001 |:white_check_mark:| `sew == 32` |
| 111  | 0x80800000 | 0x00000001 |:white_check_mark:| `sew == 32` |
| 112  | 0x80800000 | 0x80000000 |:white_check_mark:| `sew == 32` |
| 113  | 0x80800000 | 0x00000000 |:white_check_mark:| `sew == 32` |
| 114  | 0x00800000 | 0xBF800000 |:white_check_mark:| `sew == 32` |
| 115  | 0x00800000 | 0x3F800000 |:white_check_mark:| `sew == 32` |
| 116  | 0x00800000 | 0xFF7FFFFF |:white_check_mark:| `sew == 32` |
| 117  | 0x00800000 | 0x7F7FFFFF |:white_check_mark:| `sew == 32` |
| 118  | 0x00800000 | 0x80855555 |:white_check_mark:| `sew == 32` |
| 119  | 0x00800000 | 0x00800001 |:white_check_mark:| `sew == 32` |
| 120  | 0x00800000 | 0x80800000 |:white_check_mark:| `sew == 32` |
| 121  | 0x00800000 | 0x00800000 |:white_check_mark:| `sew == 32` |
| 122  | 0x00800000 | 0x807FFFFF |:white_check_mark:| `sew == 32` |
| 123  | 0x00800000 | 0x007FFFFF |:white_check_mark:| `sew == 32` |
| 124  | 0x00800000 | 0x807FFFFE |:white_check_mark:| `sew == 32` |
| 125  | 0x00800000 | 0x00000002 |:white_check_mark:| `sew == 32` |
| 126  | 0x00800000 | 0x80000001 |:white_check_mark:| `sew == 32` |
| 127  | 0x00800000 | 0x00000001 |:white_check_mark:| `sew == 32` |
| 128  | 0x00800000 | 0x80000000 |:white_check_mark:| `sew == 32` |
| 129  | 0x00800000 | 0x00000000 |:white_check_mark:| `sew == 32` |
| 130  | 0x807FFFFF | 0xBF800000 |:white_check_mark:| `sew == 32` |
| 131  | 0x807FFFFF | 0x3F800000 |:white_check_mark:| `sew == 32` |
| 132  | 0x807FFFFF | 0xFF7FFFFF |:white_check_mark:| `sew == 32` |
| 133  | 0x807FFFFF | 0x7F7FFFFF |:white_check_mark:| `sew == 32` |
| 134  | 0x807FFFFF | 0x80855555 |:white_check_mark:| `sew == 32` |
| 135  | 0x807FFFFF | 0x00800001 |:white_check_mark:| `sew == 32` |
| 136  | 0x807FFFFF | 0x80800000 |:white_check_mark:| `sew == 32` |
| 137  | 0x807FFFFF | 0x00800000 |:white_check_mark:| `sew == 32` |
| 138  | 0x807FFFFF | 0x807FFFFF |:white_check_mark:| `sew == 32` |
| 139  | 0x807FFFFF | 0x007FFFFF |:white_check_mark:| `sew == 32` |
| 140  | 0x807FFFFF | 0x807FFFFE |:white_check_mark:| `sew == 32` |
| 141  | 0x807FFFFF | 0x00000002 |:white_check_mark:| `sew == 32` |
| 142  | 0x807FFFFF | 0x80000001 |:white_check_mark:| `sew == 32` |
| 143  | 0x807FFFFF | 0x00000001 |:white_check_mark:| `sew == 32` |
| 144  | 0x807FFFFF | 0x80000000 |:white_check_mark:| `sew == 32` |
| 145  | 0x807FFFFF | 0x00000000 |:white_check_mark:| `sew == 32` |
| 146  | 0x007FFFFF | 0xBF800000 |:white_check_mark:| `sew == 32` |
| 147  | 0x007FFFFF | 0x3F800000 |:white_check_mark:| `sew == 32` |
| 148  | 0x007FFFFF | 0xFF7FFFFF |:white_check_mark:| `sew == 32` |
| 149  | 0x007FFFFF | 0x7F7FFFFF |:white_check_mark:| `sew == 32` |
| 150  | 0x007FFFFF | 0x80855555 |:white_check_mark:| `sew == 32` |
| 151  | 0x007FFFFF | 0x00800001 |:white_check_mark:| `sew == 32` |
| 152  | 0x007FFFFF | 0x80800000 |:white_check_mark:| `sew == 32` |
| 153  | 0x007FFFFF | 0x00800000 |:white_check_mark:| `sew == 32` |
| 154  | 0x007FFFFF | 0x807FFFFF |:white_check_mark:| `sew == 32` |
| 155  | 0x007FFFFF | 0x007FFFFF |:white_check_mark:| `sew == 32` |
| 156  | 0x007FFFFF | 0x807FFFFE |:white_check_mark:| `sew == 32` |
| 157  | 0x007FFFFF | 0x00000002 |:white_check_mark:| `sew == 32` |
| 158  | 0x007FFFFF | 0x80000001 |:white_check_mark:| `sew == 32` |
| 159  | 0x007FFFFF | 0x00000001 |:white_check_mark:| `sew == 32` |
| 160  | 0x007FFFFF | 0x80000000 |:white_check_mark:| `sew == 32` |
| 161  | 0x007FFFFF | 0x00000000 |:white_check_mark:| `sew == 32` |
| 162  | 0x807FFFFE | 0xBF800000 |:white_check_mark:| `sew == 32` |
| 163  | 0x807FFFFE | 0x3F800000 |:white_check_mark:| `sew == 32` |
| 164  | 0x807FFFFE | 0xFF7FFFFF |:white_check_mark:| `sew == 32` |
| 165  | 0x807FFFFE | 0x7F7FFFFF |:white_check_mark:| `sew == 32` |
| 166  | 0x807FFFFE | 0x80855555 |:white_check_mark:| `sew == 32` |
| 167  | 0x807FFFFE | 0x00800001 |:white_check_mark:| `sew == 32` |
| 168  | 0x807FFFFE | 0x80800000 |:white_check_mark:| `sew == 32` |
| 169  | 0x807FFFFE | 0x00800000 |:white_check_mark:| `sew == 32` |
| 170  | 0x807FFFFE | 0x807FFFFF |:white_check_mark:| `sew == 32` |
| 171  | 0x807FFFFE | 0x007FFFFF |:white_check_mark:| `sew == 32` |
| 172  | 0x807FFFFE | 0x807FFFFE |:white_check_mark:| `sew == 32` |
| 173  | 0x807FFFFE | 0x00000002 |:white_check_mark:| `sew == 32` |
| 174  | 0x807FFFFE | 0x80000001 |:white_check_mark:| `sew == 32` |
| 175  | 0x807FFFFE | 0x00000001 |:white_check_mark:| `sew == 32` |
| 176  | 0x807FFFFE | 0x80000000 |:white_check_mark:| `sew == 32` |
| 177  | 0x807FFFFE | 0x00000000 |:white_check_mark:| `sew == 32` |
| 178  | 0x00000002 | 0xBF800000 |:white_check_mark:| `sew == 32` |
| 179  | 0x00000002 | 0x3F800000 |:white_check_mark:| `sew == 32` |
| 180  | 0x00000002 | 0xFF7FFFFF |:white_check_mark:| `sew == 32` |
| 181  | 0x00000002 | 0x7F7FFFFF |:white_check_mark:| `sew == 32` |
| 182  | 0x00000002 | 0x80855555 |:white_check_mark:| `sew == 32` |
| 183  | 0x00000002 | 0x00800001 |:white_check_mark:| `sew == 32` |
| 184  | 0x00000002 | 0x80800000 |:white_check_mark:| `sew == 32` |
| 185  | 0x00000002 | 0x00800000 |:white_check_mark:| `sew == 32` |
| 186  | 0x00000002 | 0x807FFFFF |:white_check_mark:| `sew == 32` |
| 187  | 0x00000002 | 0x007FFFFF |:white_check_mark:| `sew == 32` |
| 188  | 0x00000002 | 0x807FFFFE |:white_check_mark:| `sew == 32` |
| 189  | 0x00000002 | 0x00000002 |:white_check_mark:| `sew == 32` |
| 190  | 0x00000002 | 0x80000001 |:white_check_mark:| `sew == 32` |
| 191  | 0x00000002 | 0x00000001 |:white_check_mark:| `sew == 32` |
| 192  | 0x00000002 | 0x80000000 |:white_check_mark:| `sew == 32` |
| 193  | 0x00000002 | 0x00000000 |:white_check_mark:| `sew == 32` |
| 194  | 0x80000001 | 0xBF800000 |:white_check_mark:| `sew == 32` |
| 195  | 0x80000001 | 0x3F800000 |:white_check_mark:| `sew == 32` |
| 196  | 0x80000001 | 0xFF7FFFFF |:white_check_mark:| `sew == 32` |
| 197  | 0x80000001 | 0x7F7FFFFF |:white_check_mark:| `sew == 32` |
| 198  | 0x80000001 | 0x80855555 |:white_check_mark:| `sew == 32` |
| 199  | 0x80000001 | 0x00800001 |:white_check_mark:| `sew == 32` |
| 200  | 0x80000001 | 0x80800000 |:white_check_mark:| `sew == 32` |
| 201  | 0x80000001 | 0x00800000 |:white_check_mark:| `sew == 32` |
| 202  | 0x80000001 | 0x807FFFFF |:white_check_mark:| `sew == 32` |
| 203  | 0x80000001 | 0x007FFFFF |:white_check_mark:| `sew == 32` |
| 204  | 0x80000001 | 0x807FFFFE |:white_check_mark:| `sew == 32` |
| 205  | 0x80000001 | 0x00000002 |:white_check_mark:| `sew == 32` |
| 206  | 0x80000001 | 0x80000001 |:white_check_mark:| `sew == 32` |
| 207  | 0x80000001 | 0x00000001 |:white_check_mark:| `sew == 32` |
| 208  | 0x80000001 | 0x80000000 |:white_check_mark:| `sew == 32` |
| 209  | 0x80000001 | 0x00000000 |:white_check_mark:| `sew == 32` |
| 210  | 0x00000001 | 0xBF800000 |:white_check_mark:| `sew == 32` |
| 211  | 0x00000001 | 0x3F800000 |:white_check_mark:| `sew == 32` |
| 212  | 0x00000001 | 0xFF7FFFFF |:white_check_mark:| `sew == 32` |
| 213  | 0x00000001 | 0x7F7FFFFF |:white_check_mark:| `sew == 32` |
| 214  | 0x00000001 | 0x80855555 |:white_check_mark:| `sew == 32` |
| 215  | 0x00000001 | 0x00800001 |:white_check_mark:| `sew == 32` |
| 216  | 0x00000001 | 0x80800000 |:white_check_mark:| `sew == 32` |
| 217  | 0x00000001 | 0x00800000 |:white_check_mark:| `sew == 32` |
| 218  | 0x00000001 | 0x807FFFFF |:white_check_mark:| `sew == 32` |
| 219  | 0x00000001 | 0x007FFFFF |:white_check_mark:| `sew == 32` |
| 220  | 0x00000001 | 0x807FFFFE |:white_check_mark:| `sew == 32` |
| 221  | 0x00000001 | 0x00000002 |:white_check_mark:| `sew == 32` |
| 222  | 0x00000001 | 0x80000001 |:white_check_mark:| `sew == 32` |
| 223  | 0x00000001 | 0x00000001 |:white_check_mark:| `sew == 32` |
| 224  | 0x00000001 | 0x80000000 |:white_check_mark:| `sew == 32` |
| 225  | 0x00000001 | 0x00000000 |:white_check_mark:| `sew == 32` |
| 226  | 0x80000000 | 0xBF800000 |:white_check_mark:| `sew == 32` |
| 227  | 0x80000000 | 0x3F800000 |:white_check_mark:| `sew == 32` |
| 228  | 0x80000000 | 0xFF7FFFFF |:white_check_mark:| `sew == 32` |
| 229  | 0x80000000 | 0x7F7FFFFF |:white_check_mark:| `sew == 32` |
| 230  | 0x80000000 | 0x80855555 |:white_check_mark:| `sew == 32` |
| 231  | 0x80000000 | 0x00800001 |:white_check_mark:| `sew == 32` |
| 232  | 0x80000000 | 0x80800000 |:white_check_mark:| `sew == 32` |
| 233  | 0x80000000 | 0x00800000 |:white_check_mark:| `sew == 32` |
| 234  | 0x80000000 | 0x807FFFFF |:white_check_mark:| `sew == 32` |
| 235  | 0x80000000 | 0x007FFFFF |:white_check_mark:| `sew == 32` |
| 236  | 0x80000000 | 0x807FFFFE |:white_check_mark:| `sew == 32` |
| 237  | 0x80000000 | 0x00000002 |:white_check_mark:| `sew == 32` |
| 238  | 0x80000000 | 0x80000001 |:white_check_mark:| `sew == 32` |
| 239  | 0x80000000 | 0x00000001 |:white_check_mark:| `sew == 32` |
| 240  | 0x80000000 | 0x80000000 |:white_check_mark:| `sew == 32` |
| 241  | 0x80000000 | 0x00000000 |:white_check_mark:| `sew == 32` |
| 242  | 0x00000000 | 0xBF800000 |:white_check_mark:| `sew == 32` |
| 243  | 0x00000000 | 0x3F800000 |:white_check_mark:| `sew == 32` |
| 244  | 0x00000000 | 0xFF7FFFFF |:white_check_mark:| `sew == 32` |
| 245  | 0x00000000 | 0x7F7FFFFF |:white_check_mark:| `sew == 32` |
| 246  | 0x00000000 | 0x80855555 |:white_check_mark:| `sew == 32` |
| 247  | 0x00000000 | 0x00800001 |:white_check_mark:| `sew == 32` |
| 248  | 0x00000000 | 0x80800000 |:white_check_mark:| `sew == 32` |
| 249  | 0x00000000 | 0x00800000 |:white_check_mark:| `sew == 32` |
| 250  | 0x00000000 | 0x807FFFFF |:white_check_mark:| `sew == 32` |
| 251  | 0x00000000 | 0x007FFFFF |:white_check_mark:| `sew == 32` |
| 252  | 0x00000000 | 0x807FFFFE |:white_check_mark:| `sew == 32` |
| 253  | 0x00000000 | 0x00000002 |:white_check_mark:| `sew == 32` |
| 254  | 0x00000000 | 0x80000001 |:white_check_mark:| `sew == 32` |
| 255  | 0x00000000 | 0x00000001 |:white_check_mark:| `sew == 32` |
| 256  | 0x00000000 | 0x80000000 |:white_check_mark:| `sew == 32` |

| No. | rs1    | rs2       | status | note |
| --- | ------ | --------- | ------ | ---- |
|   1 |  0x0000000000000000 |  0x0000000000000000 |:white_check_mark:| `sew == 64` |
|   2 |  0x0000000000000000 |  0x8000000000000000 |:white_check_mark:| `sew == 64` |
|   3 |  0x0000000000000000 |  0x0000000000000001 |:white_check_mark:| `sew == 64` |
|   4 |  0x0000000000000000 |  0x8000000000000001 |:white_check_mark:| `sew == 64` |
|   5 |  0x0000000000000000 |  0x0000000000000002 |:white_check_mark:| `sew == 64` |
|   6 |  0x0000000000000000 |  0x8000000000000002 |:white_check_mark:| `sew == 64` |
|   7 |  0x0000000000000000 |  0x000FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|   8 |  0x0000000000000000 |  0x800FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|   9 |  0x0000000000000000 |  0x0010000000000000 |:white_check_mark:| `sew == 64` |
|  10 |  0x0000000000000000 |  0x8010000000000000 |:white_check_mark:| `sew == 64` |
|  11 |  0x0000000000000000 |  0x0010000000000002 |:white_check_mark:| `sew == 64` |
|  12 |  0x0000000000000000 |  0x8010000000000002 |:white_check_mark:| `sew == 64` |
|  13 |  0x0000000000000000 |  0x7FEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  14 |  0x0000000000000000 |  0xFFEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  15 |  0x0000000000000000 |  0x3FF0000000000000 |:white_check_mark:| `sew == 64` |
|  16 |  0x0000000000000000 |  0xBF80000000000000 |:white_check_mark:| `sew == 64` |
|  17 |  0x8000000000000000 |  0x0000000000000000 |:white_check_mark:| `sew == 64` |
|  18 |  0x8000000000000000 |  0x8000000000000000 |:white_check_mark:| `sew == 64` |
|  19 |  0x8000000000000000 |  0x0000000000000001 |:white_check_mark:| `sew == 64` |
|  20 |  0x8000000000000000 |  0x8000000000000001 |:white_check_mark:| `sew == 64` |
|  21 |  0x8000000000000000 |  0x0000000000000002 |:white_check_mark:| `sew == 64` |
|  22 |  0x8000000000000000 |  0x8000000000000002 |:white_check_mark:| `sew == 64` |
|  23 |  0x8000000000000000 |  0x000FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  24 |  0x8000000000000000 |  0x800FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  25 |  0x8000000000000000 |  0x0010000000000000 |:white_check_mark:| `sew == 64` |
|  26 |  0x8000000000000000 |  0x8010000000000000 |:white_check_mark:| `sew == 64` |
|  27 |  0x8000000000000000 |  0x0010000000000002 |:white_check_mark:| `sew == 64` |
|  28 |  0x8000000000000000 |  0x8010000000000002 |:white_check_mark:| `sew == 64` |
|  29 |  0x8000000000000000 |  0x7FEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  30 |  0x8000000000000000 |  0xFFEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  31 |  0x8000000000000000 |  0x3FF0000000000000 |:white_check_mark:| `sew == 64` |
|  32 |  0x8000000000000000 |  0xBF80000000000000 |:white_check_mark:| `sew == 64` |
|  33 |  0x0000000000000001 |  0x0000000000000000 |:white_check_mark:| `sew == 64` |
|  34 |  0x0000000000000001 |  0x8000000000000000 |:white_check_mark:| `sew == 64` |
|  35 |  0x0000000000000001 |  0x0000000000000001 |:white_check_mark:| `sew == 64` |
|  36 |  0x0000000000000001 |  0x8000000000000001 |:white_check_mark:| `sew == 64` |
|  37 |  0x0000000000000001 |  0x0000000000000002 |:white_check_mark:| `sew == 64` |
|  38 |  0x0000000000000001 |  0x8000000000000002 |:white_check_mark:| `sew == 64` |
|  39 |  0x0000000000000001 |  0x000FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  40 |  0x0000000000000001 |  0x800FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  41 |  0x0000000000000001 |  0x0010000000000000 |:white_check_mark:| `sew == 64` |
|  42 |  0x0000000000000001 |  0x8010000000000000 |:white_check_mark:| `sew == 64` |
|  43 |  0x0000000000000001 |  0x0010000000000002 |:white_check_mark:| `sew == 64` |
|  44 |  0x0000000000000001 |  0x8010000000000002 |:white_check_mark:| `sew == 64` |
|  45 |  0x0000000000000001 |  0x7FEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  46 |  0x0000000000000001 |  0xFFEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  47 |  0x0000000000000001 |  0x3FF0000000000000 |:white_check_mark:| `sew == 64` |
|  48 |  0x0000000000000001 |  0xBF80000000000000 |:white_check_mark:| `sew == 64` |
|  49 |  0x8000000000000001 |  0x0000000000000000 |:white_check_mark:| `sew == 64` |
|  50 |  0x8000000000000001 |  0x8000000000000000 |:white_check_mark:| `sew == 64` |
|  51 |  0x8000000000000001 |  0x0000000000000001 |:white_check_mark:| `sew == 64` |
|  52 |  0x8000000000000001 |  0x8000000000000001 |:white_check_mark:| `sew == 64` |
|  53 |  0x8000000000000001 |  0x0000000000000002 |:white_check_mark:| `sew == 64` |
|  54 |  0x8000000000000001 |  0x8000000000000002 |:white_check_mark:| `sew == 64` |
|  55 |  0x8000000000000001 |  0x000FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  56 |  0x8000000000000001 |  0x800FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  57 |  0x8000000000000001 |  0x0010000000000000 |:white_check_mark:| `sew == 64` |
|  58 |  0x8000000000000001 |  0x8010000000000000 |:white_check_mark:| `sew == 64` |
|  59 |  0x8000000000000001 |  0x0010000000000002 |:white_check_mark:| `sew == 64` |
|  60 |  0x8000000000000001 |  0x8010000000000002 |:white_check_mark:| `sew == 64` |
|  61 |  0x8000000000000001 |  0x7FEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  62 |  0x8000000000000001 |  0xFFEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  63 |  0x8000000000000001 |  0x3FF0000000000000 |:white_check_mark:| `sew == 64` |
|  64 |  0x8000000000000001 |  0xBF80000000000000 |:white_check_mark:| `sew == 64` |
|  65 |  0x0000000000000002 |  0x0000000000000000 |:white_check_mark:| `sew == 64` |
|  66 |  0x0000000000000002 |  0x8000000000000000 |:white_check_mark:| `sew == 64` |
|  67 |  0x0000000000000002 |  0x0000000000000001 |:white_check_mark:| `sew == 64` |
|  68 |  0x0000000000000002 |  0x8000000000000001 |:white_check_mark:| `sew == 64` |
|  69 |  0x0000000000000002 |  0x0000000000000002 |:white_check_mark:| `sew == 64` |
|  70 |  0x0000000000000002 |  0x8000000000000002 |:white_check_mark:| `sew == 64` |
|  71 |  0x0000000000000002 |  0x000FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  72 |  0x0000000000000002 |  0x800FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  73 |  0x0000000000000002 |  0x0010000000000000 |:white_check_mark:| `sew == 64` |
|  74 |  0x0000000000000002 |  0x8010000000000000 |:white_check_mark:| `sew == 64` |
|  75 |  0x0000000000000002 |  0x0010000000000002 |:white_check_mark:| `sew == 64` |
|  76 |  0x0000000000000002 |  0x8010000000000002 |:white_check_mark:| `sew == 64` |
|  77 |  0x0000000000000002 |  0x7FEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  78 |  0x0000000000000002 |  0xFFEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  79 |  0x0000000000000002 |  0x3FF0000000000000 |:white_check_mark:| `sew == 64` |
|  80 |  0x0000000000000002 |  0xBF80000000000000 |:white_check_mark:| `sew == 64` |
|  81 |  0x8000000000000002 |  0x0000000000000000 |:white_check_mark:| `sew == 64` |
|  82 |  0x8000000000000002 |  0x8000000000000000 |:white_check_mark:| `sew == 64` |
|  83 |  0x8000000000000002 |  0x0000000000000001 |:white_check_mark:| `sew == 64` |
|  84 |  0x8000000000000002 |  0x8000000000000001 |:white_check_mark:| `sew == 64` |
|  85 |  0x8000000000000002 |  0x0000000000000002 |:white_check_mark:| `sew == 64` |
|  86 |  0x8000000000000002 |  0x8000000000000002 |:white_check_mark:| `sew == 64` |
|  87 |  0x8000000000000002 |  0x000FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  88 |  0x8000000000000002 |  0x800FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  89 |  0x8000000000000002 |  0x0010000000000000 |:white_check_mark:| `sew == 64` |
|  90 |  0x8000000000000002 |  0x8010000000000000 |:white_check_mark:| `sew == 64` |
|  91 |  0x8000000000000002 |  0x0010000000000002 |:white_check_mark:| `sew == 64` |
|  92 |  0x8000000000000002 |  0x8010000000000002 |:white_check_mark:| `sew == 64` |
|  93 |  0x8000000000000002 |  0x7FEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  94 |  0x8000000000000002 |  0xFFEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
|  95 |  0x8000000000000002 |  0x3FF0000000000000 |:white_check_mark:| `sew == 64` |
|  96 |  0x8000000000000002 |  0xBF80000000000000 |:white_check_mark:| `sew == 64` |
|  97 |  0x000FFFFFFFFFFFFF |  0x0000000000000000 |:white_check_mark:| `sew == 64` |
|  98 |  0x000FFFFFFFFFFFFF |  0x8000000000000000 |:white_check_mark:| `sew == 64` |
|  99 |  0x000FFFFFFFFFFFFF |  0x0000000000000001 |:white_check_mark:| `sew == 64` |
| 100 |  0x000FFFFFFFFFFFFF |  0x8000000000000001 |:white_check_mark:| `sew == 64` |
| 101 |  0x000FFFFFFFFFFFFF |  0x0000000000000002 |:white_check_mark:| `sew == 64` |
| 102 |  0x000FFFFFFFFFFFFF |  0x8000000000000002 |:white_check_mark:| `sew == 64` |
| 103 |  0x000FFFFFFFFFFFFF |  0x000FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 104 |  0x000FFFFFFFFFFFFF |  0x800FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 105 |  0x000FFFFFFFFFFFFF |  0x0010000000000000 |:white_check_mark:| `sew == 64` |
| 106 |  0x000FFFFFFFFFFFFF |  0x8010000000000000 |:white_check_mark:| `sew == 64` |
| 107 |  0x000FFFFFFFFFFFFF |  0x0010000000000002 |:white_check_mark:| `sew == 64` |
| 108 |  0x000FFFFFFFFFFFFF |  0x8010000000000002 |:white_check_mark:| `sew == 64` |
| 109 |  0x000FFFFFFFFFFFFF |  0x7FEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 110 |  0x000FFFFFFFFFFFFF |  0xFFEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 111 |  0x000FFFFFFFFFFFFF |  0x3FF0000000000000 |:white_check_mark:| `sew == 64` |
| 112 |  0x000FFFFFFFFFFFFF |  0xBF80000000000000 |:white_check_mark:| `sew == 64` |
| 113 |  0x800FFFFFFFFFFFFF |  0x0000000000000000 |:white_check_mark:| `sew == 64` |
| 114 |  0x800FFFFFFFFFFFFF |  0x8000000000000000 |:white_check_mark:| `sew == 64` |
| 115 |  0x800FFFFFFFFFFFFF |  0x0000000000000001 |:white_check_mark:| `sew == 64` |
| 116 |  0x800FFFFFFFFFFFFF |  0x8000000000000001 |:white_check_mark:| `sew == 64` |
| 117 |  0x800FFFFFFFFFFFFF |  0x0000000000000002 |:white_check_mark:| `sew == 64` |
| 118 |  0x800FFFFFFFFFFFFF |  0x8000000000000002 |:white_check_mark:| `sew == 64` |
| 119 |  0x800FFFFFFFFFFFFF |  0x000FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 120 |  0x800FFFFFFFFFFFFF |  0x800FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 121 |  0x800FFFFFFFFFFFFF |  0x0010000000000000 |:white_check_mark:| `sew == 64` |
| 122 |  0x800FFFFFFFFFFFFF |  0x8010000000000000 |:white_check_mark:| `sew == 64` |
| 123 |  0x800FFFFFFFFFFFFF |  0x0010000000000002 |:white_check_mark:| `sew == 64` |
| 124 |  0x800FFFFFFFFFFFFF |  0x8010000000000002 |:white_check_mark:| `sew == 64` |
| 125 |  0x800FFFFFFFFFFFFF |  0x7FEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 126 |  0x800FFFFFFFFFFFFF |  0xFFEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 127 |  0x800FFFFFFFFFFFFF |  0x3FF0000000000000 |:white_check_mark:| `sew == 64` |
| 128 |  0x800FFFFFFFFFFFFF |  0xBF80000000000000 |:white_check_mark:| `sew == 64` |
| 129 |  0x0010000000000000 |  0x0000000000000000 |:white_check_mark:| `sew == 64` |
| 130 |  0x0010000000000000 |  0x8000000000000000 |:white_check_mark:| `sew == 64` |
| 131 |  0x0010000000000000 |  0x0000000000000001 |:white_check_mark:| `sew == 64` |
| 132 |  0x0010000000000000 |  0x8000000000000001 |:white_check_mark:| `sew == 64` |
| 133 |  0x0010000000000000 |  0x0000000000000002 |:white_check_mark:| `sew == 64` |
| 134 |  0x0010000000000000 |  0x8000000000000002 |:white_check_mark:| `sew == 64` |
| 135 |  0x0010000000000000 |  0x000FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 136 |  0x0010000000000000 |  0x800FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 137 |  0x0010000000000000 |  0x0010000000000000 |:white_check_mark:| `sew == 64` |
| 138 |  0x0010000000000000 |  0x8010000000000000 |:white_check_mark:| `sew == 64` |
| 139 |  0x0010000000000000 |  0x0010000000000002 |:white_check_mark:| `sew == 64` |
| 140 |  0x0010000000000000 |  0x8010000000000002 |:white_check_mark:| `sew == 64` |
| 141 |  0x0010000000000000 |  0x7FEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 142 |  0x0010000000000000 |  0xFFEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 143 |  0x0010000000000000 |  0x3FF0000000000000 |:white_check_mark:| `sew == 64` |
| 144 |  0x0010000000000000 |  0xBF80000000000000 |:white_check_mark:| `sew == 64` |
| 145 |  0x8010000000000000 |  0x0000000000000000 |:white_check_mark:| `sew == 64` |
| 146 |  0x8010000000000000 |  0x8000000000000000 |:white_check_mark:| `sew == 64` |
| 147 |  0x8010000000000000 |  0x0000000000000001 |:white_check_mark:| `sew == 64` |
| 148 |  0x8010000000000000 |  0x8000000000000001 |:white_check_mark:| `sew == 64` |
| 149 |  0x8010000000000000 |  0x0000000000000002 |:white_check_mark:| `sew == 64` |
| 150 |  0x8010000000000000 |  0x8000000000000002 |:white_check_mark:| `sew == 64` |
| 151 |  0x8010000000000000 |  0x000FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 152 |  0x8010000000000000 |  0x800FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 153 |  0x8010000000000000 |  0x0010000000000000 |:white_check_mark:| `sew == 64` |
| 154 |  0x8010000000000000 |  0x8010000000000000 |:white_check_mark:| `sew == 64` |
| 155 |  0x8010000000000000 |  0x0010000000000002 |:white_check_mark:| `sew == 64` |
| 156 |  0x8010000000000000 |  0x8010000000000002 |:white_check_mark:| `sew == 64` |
| 157 |  0x8010000000000000 |  0x7FEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 158 |  0x8010000000000000 |  0xFFEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 159 |  0x8010000000000000 |  0x3FF0000000000000 |:white_check_mark:| `sew == 64` |
| 160 |  0x8010000000000000 |  0xBF80000000000000 |:white_check_mark:| `sew == 64` |
| 161 |  0x0010000000000002 |  0x0000000000000000 |:white_check_mark:| `sew == 64` |
| 162 |  0x0010000000000002 |  0x8000000000000000 |:white_check_mark:| `sew == 64` |
| 163 |  0x0010000000000002 |  0x0000000000000001 |:white_check_mark:| `sew == 64` |
| 164 |  0x0010000000000002 |  0x8000000000000001 |:white_check_mark:| `sew == 64` |
| 165 |  0x0010000000000002 |  0x0000000000000002 |:white_check_mark:| `sew == 64` |
| 166 |  0x0010000000000002 |  0x8000000000000002 |:white_check_mark:| `sew == 64` |
| 167 |  0x0010000000000002 |  0x000FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 168 |  0x0010000000000002 |  0x800FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 169 |  0x0010000000000002 |  0x0010000000000000 |:white_check_mark:| `sew == 64` |
| 170 |  0x0010000000000002 |  0x8010000000000000 |:white_check_mark:| `sew == 64` |
| 171 |  0x0010000000000002 |  0x0010000000000002 |:white_check_mark:| `sew == 64` |
| 172 |  0x0010000000000002 |  0x8010000000000002 |:white_check_mark:| `sew == 64` |
| 173 |  0x0010000000000002 |  0x7FEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 174 |  0x0010000000000002 |  0xFFEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 175 |  0x0010000000000002 |  0x3FF0000000000000 |:white_check_mark:| `sew == 64` |
| 176 |  0x0010000000000002 |  0xBF80000000000000 |:white_check_mark:| `sew == 64` |
| 177 |  0x8010000000000002 |  0x0000000000000000 |:white_check_mark:| `sew == 64` |
| 178 |  0x8010000000000002 |  0x8000000000000000 |:white_check_mark:| `sew == 64` |
| 179 |  0x8010000000000002 |  0x0000000000000001 |:white_check_mark:| `sew == 64` |
| 180 |  0x8010000000000002 |  0x8000000000000001 |:white_check_mark:| `sew == 64` |
| 181 |  0x8010000000000002 |  0x0000000000000002 |:white_check_mark:| `sew == 64` |
| 182 |  0x8010000000000002 |  0x8000000000000002 |:white_check_mark:| `sew == 64` |
| 183 |  0x8010000000000002 |  0x000FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 184 |  0x8010000000000002 |  0x800FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 185 |  0x8010000000000002 |  0x0010000000000000 |:white_check_mark:| `sew == 64` |
| 186 |  0x8010000000000002 |  0x8010000000000000 |:white_check_mark:| `sew == 64` |
| 187 |  0x8010000000000002 |  0x0010000000000002 |:white_check_mark:| `sew == 64` |
| 188 |  0x8010000000000002 |  0x8010000000000002 |:white_check_mark:| `sew == 64` |
| 189 |  0x8010000000000002 |  0x7FEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 190 |  0x8010000000000002 |  0xFFEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 191 |  0x8010000000000002 |  0x3FF0000000000000 |:white_check_mark:| `sew == 64` |
| 192 |  0x8010000000000002 |  0xBF80000000000000 |:white_check_mark:| `sew == 64` |
| 193 |  0x7FEFFFFFFFFFFFFF |  0x0000000000000000 |:white_check_mark:| `sew == 64` |
| 194 |  0x7FEFFFFFFFFFFFFF |  0x8000000000000000 |:white_check_mark:| `sew == 64` |
| 195 |  0x7FEFFFFFFFFFFFFF |  0x0000000000000001 |:white_check_mark:| `sew == 64` |
| 196 |  0x7FEFFFFFFFFFFFFF |  0x8000000000000001 |:white_check_mark:| `sew == 64` |
| 197 |  0x7FEFFFFFFFFFFFFF |  0x0000000000000002 |:white_check_mark:| `sew == 64` |
| 198 |  0x7FEFFFFFFFFFFFFF |  0x8000000000000002 |:white_check_mark:| `sew == 64` |
| 199 |  0x7FEFFFFFFFFFFFFF |  0x000FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 200 |  0x7FEFFFFFFFFFFFFF |  0x800FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 201 |  0x7FEFFFFFFFFFFFFF |  0x0010000000000000 |:white_check_mark:| `sew == 64` |
| 202 |  0x7FEFFFFFFFFFFFFF |  0x8010000000000000 |:white_check_mark:| `sew == 64` |
| 203 |  0x7FEFFFFFFFFFFFFF |  0x0010000000000002 |:white_check_mark:| `sew == 64` |
| 204 |  0x7FEFFFFFFFFFFFFF |  0x8010000000000002 |:white_check_mark:| `sew == 64` |
| 205 |  0x7FEFFFFFFFFFFFFF |  0x7FEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 206 |  0x7FEFFFFFFFFFFFFF |  0xFFEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 207 |  0x7FEFFFFFFFFFFFFF |  0x3FF0000000000000 |:white_check_mark:| `sew == 64` |
| 208 |  0x7FEFFFFFFFFFFFFF |  0xBF80000000000000 |:white_check_mark:| `sew == 64` |
| 209 |  0xFFEFFFFFFFFFFFFF |  0x0000000000000000 |:white_check_mark:| `sew == 64` |
| 210 |  0xFFEFFFFFFFFFFFFF |  0x8000000000000000 |:white_check_mark:| `sew == 64` |
| 211 |  0xFFEFFFFFFFFFFFFF |  0x0000000000000001 |:white_check_mark:| `sew == 64` |
| 212 |  0xFFEFFFFFFFFFFFFF |  0x8000000000000001 |:white_check_mark:| `sew == 64` |
| 213 |  0xFFEFFFFFFFFFFFFF |  0x0000000000000002 |:white_check_mark:| `sew == 64` |
| 214 |  0xFFEFFFFFFFFFFFFF |  0x8000000000000002 |:white_check_mark:| `sew == 64` |
| 215 |  0xFFEFFFFFFFFFFFFF |  0x000FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 216 |  0xFFEFFFFFFFFFFFFF |  0x800FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 217 |  0xFFEFFFFFFFFFFFFF |  0x0010000000000000 |:white_check_mark:| `sew == 64` |
| 218 |  0xFFEFFFFFFFFFFFFF |  0x8010000000000000 |:white_check_mark:| `sew == 64` |
| 219 |  0xFFEFFFFFFFFFFFFF |  0x0010000000000002 |:white_check_mark:| `sew == 64` |
| 220 |  0xFFEFFFFFFFFFFFFF |  0x8010000000000002 |:white_check_mark:| `sew == 64` |
| 221 |  0xFFEFFFFFFFFFFFFF |  0x7FEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 222 |  0xFFEFFFFFFFFFFFFF |  0xFFEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 223 |  0xFFEFFFFFFFFFFFFF |  0x3FF0000000000000 |:white_check_mark:| `sew == 64` |
| 224 |  0xFFEFFFFFFFFFFFFF |  0xBF80000000000000 |:white_check_mark:| `sew == 64` |
| 225 |  0x3FF0000000000000 |  0x0000000000000000 |:white_check_mark:| `sew == 64` |
| 226 |  0x3FF0000000000000 |  0x8000000000000000 |:white_check_mark:| `sew == 64` |
| 227 |  0x3FF0000000000000 |  0x0000000000000001 |:white_check_mark:| `sew == 64` |
| 228 |  0x3FF0000000000000 |  0x8000000000000001 |:white_check_mark:| `sew == 64` |
| 229 |  0x3FF0000000000000 |  0x0000000000000002 |:white_check_mark:| `sew == 64` |
| 230 |  0x3FF0000000000000 |  0x8000000000000002 |:white_check_mark:| `sew == 64` |
| 231 |  0x3FF0000000000000 |  0x000FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 232 |  0x3FF0000000000000 |  0x800FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 233 |  0x3FF0000000000000 |  0x0010000000000000 |:white_check_mark:| `sew == 64` |
| 234 |  0x3FF0000000000000 |  0x8010000000000000 |:white_check_mark:| `sew == 64` |
| 235 |  0x3FF0000000000000 |  0x0010000000000002 |:white_check_mark:| `sew == 64` |
| 236 |  0x3FF0000000000000 |  0x8010000000000002 |:white_check_mark:| `sew == 64` |
| 237 |  0x3FF0000000000000 |  0x7FEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 238 |  0x3FF0000000000000 |  0xFFEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 239 |  0x3FF0000000000000 |  0x3FF0000000000000 |:white_check_mark:| `sew == 64` |
| 240 |  0x3FF0000000000000 |  0xBF80000000000000 |:white_check_mark:| `sew == 64` |
| 241 |  0xBF80000000000000 |  0x0000000000000000 |:white_check_mark:| `sew == 64` |
| 242 |  0xBF80000000000000 |  0x8000000000000000 |:white_check_mark:| `sew == 64` |
| 243 |  0xBF80000000000000 |  0x0000000000000001 |:white_check_mark:| `sew == 64` |
| 244 |  0xBF80000000000000 |  0x8000000000000001 |:white_check_mark:| `sew == 64` |
| 245 |  0xBF80000000000000 |  0x0000000000000002 |:white_check_mark:| `sew == 64` |
| 246 |  0xBF80000000000000 |  0x8000000000000002 |:white_check_mark:| `sew == 64` |
| 247 |  0xBF80000000000000 |  0x000FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 248 |  0xBF80000000000000 |  0x800FFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 249 |  0xBF80000000000000 |  0x0010000000000000 |:white_check_mark:| `sew == 64` |
| 250 |  0xBF80000000000000 |  0x8010000000000000 |:white_check_mark:| `sew == 64` |
| 251 |  0xBF80000000000000 |  0x0010000000000002 |:white_check_mark:| `sew == 64` |
| 252 |  0xBF80000000000000 |  0x8010000000000002 |:white_check_mark:| `sew == 64` |
| 253 |  0xBF80000000000000 |  0x7FEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 254 |  0xBF80000000000000 |  0xFFEFFFFFFFFFFFFF |:white_check_mark:| `sew == 64` |
| 255 |  0xBF80000000000000 |  0x3FF0000000000000 |:white_check_mark:| `sew == 64` |
| 256 |  0xBF80000000000000 |  0xBF80000000000000 |:white_check_mark:| `sew == 64` |
> **Note**:
>  For division instruction like `vdiv`, `vfdiv`, etc. We remove all `zero` from source value lists.

### Instruction type: Load/Store 
| No.| inst type |  result width | result | memory address | status | 
| -- | --------- | ------------- | ------ | -------------- | ------ |
| 1  | load      |  8            |  0xaa  |  0 + tdat      | :white_check_mark:|
| 2  | load      |  8            |  0x00  |  -8 + tdat8    | :white_check_mark:|
| 3  | store     |  8            |  0xaa  |  0 + tdat      | :white_check_mark:|
| 4  | store     |  8            |  0x00  |  -8 + tdat8    | :white_check_mark:|

> **Note:**
> The memory address of `tdta` is `0x00ff00ff` and `tdat8` is `0xf00ff00f`. The names of `tdta` and `tdat8`, are notations that can be modified from code base.
> The result can be any valid value, the above cases just show two , `0x00` and `0xaa` when result width is equal to `8`.
> The result width can also be `16` or `32`. It is configurable.

### Instruction type: Mask/Permute
| No. |  rs1 | rs2 | status |
| --- | ---------------------- | ----------------- |----------------- |
|  1  |  walking_ones_vid_ans0 | walking_ones_dat0 |:white_check_mark:|
|  2  |  walking_zeros_vid_ans0| walking_zeros_dat0|:white_check_mark:|
|  3  |  walking_ones_vid_ans1 | walking_ones_dat1 |:white_check_mark:|
|  4  |  walking_zeros_vid_ans1| walking_zeros_dat1|:white_check_mark:|
|  5  |  walking_ones_vid_ans2 | walking_ones_dat2 |:white_check_mark:|
|  6  |  walking_zeros_vid_ans2| walking_zeros_dat2|:white_check_mark:|
|  7  |  walking_ones_vid_ans3 | walking_ones_dat3 |:white_check_mark:|
|  8  |  walking_zeros_vid_ans3| walking_zeros_dat3|:white_check_mark:|
|  9  |  walking_ones_vid_ans4 | walking_ones_dat4 |:white_check_mark:|
| 10  |  walking_zeros_vid_ans4| walking_zeros_dat4|:white_check_mark:|

The table below shows the value of the notation in the above table. 
| No. | notation | value | 
| --- | -------- | ----- |
|  1 | walking_ones_vid_ans0 | `.word	0x0 .word	0x0 .word	0x0 .word	0x0` |
|  2 | walking_ones_vid_ans0 | `.word	0 .word	1 .word	2 .word	3` |
|  3 | walking_ones_vid_ans1 | ```.word	0 .word	0x0 .word	0x0 .word	0x0```|
|  4 | walking_zeros_vid_ans1| `.word	0x0 .word	1 .word	2 .word	3`|
|  5 | walking_ones_vid_ans2 | `.word	0x0 .word	1 .word	0x0 .word	0x0`|
|  6 | walking_zeros_vid_ans2| `.word	0 .word	0x0 .word	2 .word	3 `|
|  7 | walking_ones_vid_ans3 | `.word	0x0 .word	0x0 .word	2 .word	0x0` |
|  8 | walking_zeros_vid_ans3| `.word	0 .word	1 .word	0x0 .word	3 ` |
|  9 | walking_ones_vid_ans4 | `.word	0x0 .word	0x0 .word	0x0 .word	3` |
| 10 | walking_zeros_vid_ans4| `.word	0 .word	1 .word	2 .word	0x0` |
| 11 | walking_ones_dat0     | `.word	0x0 .word	0x0 .word	0x0 .word	0x0` |
| 12 | walking_zeros_dat0    | `.word	0x1 .word	0x1 .word	0x1 .word	0x1` |
| 13 | walking_ones_dat1     | `.word	0x1 .word	0x0 .word	0x0 .word	0x0` |
| 14 | walking_zeros_dat1    | `.word	0x0 .word	0x1 .word	0x1 .word	0x1` |
| 15 | walking_ones_dat2     | `.word	0x0 .word	0x1 .word	0x0 .word	0x0` |
| 16 | walking_zeros_dat2    | `.word	0x1 .word	0x0 .word	0x1 .word	0x1` |
| 17 | walking_ones_dat3     | `.word	0x0 .word	0x0 .word	0x1 .word	0x0` |
| 18 | walking_zeros_dat3    | `.word	0x1 .word	0x1 .word	0x0 .word	0x1` |
| 19 | walking_ones_dat4     | `.word	0x0 .word	0x0 .word	0x0 .word	0x1` |
| 20 | walking_zeros_dat4    | `.word	0x1 .word	0x1 .word	0x1 .word	0x0` |

## Appendix C: Concepts Explanation
### Register Alignment
Multiple vector registers can be grouped so that a single instruction can operate on multiple vector registers. The vector length multiplier, LMUL, when greater than `1`, represents the default number of vector registers that combined to form a vector register group. Hence, in some cases, for example, when `LMUL = 2` or `LMUL = 4` or `LMUL = 8`, we could not cover all the registers from `v0` to `v31`.

RVVATG supports specific condition checkers to avoid unnecessary overlap among registers. For example, the checker function `require_noover(insn.rd(), rd_lmul, insn.rs1(), rs1_lmul)` requires `rs1` and `rd` will not overwrite each other. Specifically, no overlap condition means either rightest `rs1` is smaller than `rd` or rightmost `rd` is smaller than `rs1`. That is `((rs1 + rs1_lmul - 1 < rd) or (rd + rd_lmul - 1 < rs1))`. 

### Fill Empty Tests
RVVATG supports empty tests firstly, with only head and tail. After the empty tests pass, then RVVATG generates tests with the descriptive body as well as head and tail. The following snippet shows how an empty test's ending is generated.
```python
def print_ending(f):
    print("  RVTEST_SIGBASE( x20,signature_x20_2)\n\
    \n\
    TEST_VV_OP(32766, vadd.vv, 2, 1, 1)\n\
    TEST_PASSFAIL\n\
    #endif\n\
```
The `TEST_VV_OP(32766, vadd.vv, 2, 1, 1)` is to fill those tests that do not have tests(For example, when`emul > 8`, load/store will not have tests) with one `vadd` test.
